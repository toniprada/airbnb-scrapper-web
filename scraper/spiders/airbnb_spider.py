import scrapy
import json
import time
import csv
from scraper.items import UserItem
from scraper import settings
from bs4 import BeautifulSoup
import re
import requests
import logging
import random

class AirbnbSpider(scrapy.Spider):
    name = "airbnb"
    allowed_domains = ["airbnb.com"]

    def __init__(self, *args, **kwargs):
        super(AirbnbSpider, self).__init__(*args, **kwargs)
        self.start_urls = self.users_to_download()
        with open('input/proxies.txt') as f:
            self.proxies = [x.strip() for x in f.readlines()]

    def users_to_download(self):
        num = 0
        while num < 191400000:
            yield 'https://airbnb.com/users/show/%s' % num
            num += 1

    def parse(self, response):
        soup = BeautifulSoup(response.body, 'html.parser')
        # id
        user = { }
        for link in soup.find_all('link'):
            match = re.search(r'airbnb\.com\/users\/show\/([0-9]+)', link['href'], re.IGNORECASE)
            if match:
                user['id'] = match.group(1)
                logging.warning(user['id'])
                break
        # name and identity
        match = re.search(r'"verifications_tooltip":({.*?})', response.body, re.IGNORECASE)
        if match:
            user.update(json.loads(match.group(1)))
        # location
        e = soup.select('.page-container > div > div > div > div > div.h5.space-top-2 > a')
        if e and len(e) > 0: user['location'] = e[0].text
        # signup date
        e = soup.select('.page-container > div > div > div > div > div > span')
        if e and len(e) > 0: user['signup_date'] = e[0].text.replace("Joined in", "").strip()
        # description
        e = soup.select('.page-container > div > div > div > p')
        if e and len(e) > 0: user['description'] = e[0].text.strip()
        # reviews count
        e = soup.select('#undefined_count > span')
        if e and len(e) > 0: user['reviews_count'] = int(e[0].text.strip())
        # image
        e = soup.select('.media-photo img')
        if e and len(e) > 0:
            for x in e:
                if x['src']:
                    user['image'] = x['src']
                    break
        # verifications
        e = soup.select('.page-container > div > div > div > div.panel-body > ul > li > div:nth-of-type(1)')
        if e and len(e) > 0: user['verifications'] = [x.text.strip() for x in e]
        # about
        keys = soup.select('.page-container > div > div > div:nth-of-type(4) > div > dl > dt')
        values = soup.select('.page-container > div > div > div:nth-of-type(4) > div > dl > dd')
        if keys and len(keys) > 0 and values and len(keys) == len(values):
            about = {}
            for i in range(len(keys)):
                about[keys[i].text.strip()] = values[i].text.strip()
            user['about'] = about
        # reviews in soup
        self._set_base_html_reviews(user, soup, '.reviews_section.as_host', 'reviews_as_host')
        self._set_base_html_reviews(user, soup, '.reviews_section.as_guest', 'reviews_as_guest')
        self._set_base_html_reviews(user, soup, '#references', 'references')
        yield self._host_reviews_pagination_generator(user, 'load_more_reviews_as_host')

    def _host_reviews_pagination_generator(self, user, load_more_key, next_page=2):
        should_load_more = load_more_key in user and user[load_more_key]
        user.pop(load_more_key, None)
        if should_load_more:
            url = 'https://www.airbnb.com/users/review_page/%s?page=%s&role=host' % (user['id'], next_page)
            return scrapy.Request(url, callback=self._host_review_pagination_callback,
                                       meta={'user': user, 'page': next_page})
        return self._guest_reviews_pagination_generator(user, 'load_more_reviews_as_guest')

    def _host_review_pagination_callback(self, response):
        key = 'reviews_as_host'
        user = response.meta['user']
        page = response.meta['page']
        content = json.loads(response.body)
        if 'review_content' in content:
            soup = BeautifulSoup(content['review_content'], 'html.parser')
            user[key] += self._extract_reviews_from_soup(soup)
            user['load_more_%s' % key] = content['last_page'] is False
            yield self._host_reviews_pagination_generator(user, 'load_more_%s' % key, next_page=page+1)
        else:
            yield self._host_reviews_pagination_generator(user, 'non-existing-key')

    ##### GUEST REVIEWS

    def _guest_reviews_pagination_generator(self, user, load_more_key, next_page=2):
        should_load_more = load_more_key in user and user[load_more_key]
        user.pop(load_more_key, None)
        if should_load_more:
            url = 'https://www.airbnb.com/users/review_page/%s?page=%s&role=guest' % (user['id'], next_page)
            return scrapy.Request(url, callback=self._guest_review_pagination_callback,
                                       meta={'user': user, 'page': next_page})
        return self._references_pagination_generator(user, 'load_more_references')

    def _guest_review_pagination_callback(self, response):
        key = 'reviews_as_guest'
        user = response.meta['user']
        page = response.meta['page']
        content = json.loads(response.body)
        if 'review_content' in content:
            soup = BeautifulSoup(content['review_content'], 'html.parser')
            user[key] += self._extract_reviews_from_soup(soup)
            user['load_more_%s' % key] = content['last_page'] is False
            yield self._guest_reviews_pagination_generator(user, 'load_more_%s' % key, next_page=page+1)
        else:
            yield self._guest_reviews_pagination_generator(user, 'non-existing-key')

    ##### REFERENCES

    def _references_pagination_generator(self, user, load_more_key, next_page=2):
        should_load_more = load_more_key in user and user[load_more_key]
        user.pop(load_more_key, None)
        if should_load_more:
            url = 'https://www.airbnb.com/users/recommendation_page/%s?page=%s' % (user['id'], next_page)
            return scrapy.Request(url, callback=self._references_pagination_callback,
                                       meta={'user': user, 'page': next_page})
        item = UserItem()
        item['created_at'] = time.time()
        item['user']       = user
        return item

    def _references_pagination_callback(self, response):
        key = 'references'
        user = response.meta['user']
        page = response.meta['page']
        content = json.loads(response.body)
        if 'recommendation_content' in content:
            soup = BeautifulSoup(content['recommendation_content'], 'html.parser')
            user[key] += self._extract_reviews_from_soup(soup)
            user['load_more_%s' % key] = content['last_page'] is False
            yield self._references_pagination_generator(user, 'load_more_%s' % key, next_page=page+1)
        else:
            yield self._references_pagination_generator(user, 'non-existing-key')

    # private methods

    def _set_base_html_reviews(self, user, soup, selector, key):
        reviews = self._extract_reviews_from_soup(soup, selector=selector)
        if reviews:
            user[key] = reviews
            user['load_more_%s' % key] = self._are_there_more_reviews(soup, selector)

    def _extract_reviews_from_soup(self, soup, selector=None):
        css = '.row.text-center-sm'
        if selector:
            css =  selector + ' ' + css
        elements = soup.select(css)
        if elements:
            return [self._parse_review(e) for e in elements]

    def _are_there_more_reviews(self, soup, selector):
        return bool(soup.select('%s .load_more' % selector))

    def _parse_review(self, element):
        review = {}
        review['text'] = element.select_one('p').text.strip()
        x = element.select_one('.avatar-wrapper a')
        if x:
            review['reviewer_id'] = x['href'].replace('/users/show/', '').strip()
        else:
            x = element.select_one('.avatar-wrapper img')
            if 'data-original' in x:
                match = re.search(r'users\/([0-9]+)\/profile_pic', x['data-original'], re.IGNORECASE)
                if match:
                    review['reviewer_id'] = match.group(1)
                else:
                    review["FAILURE"] = str(element)
        # location
        x  = element.select_one('.date a')
        if x: review['location'] = x.text.strip()
        # date
        x  = element.select_one('.date')
        if x and x.text.strip(): review['date'] = x.text
        return review
