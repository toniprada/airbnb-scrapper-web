# airbnb-scraper

Parallel downloading of airbnb profiles.

## How to

To run this project you will need to:

1. Fill the method `user_ids_to_download` at airbnb_spider.py with the strategy you need to download the data (*as a list of ids coming from a CSV file*).
3. Execute `scrapy crawl airbnb`. Profiles will be saved as JSON files.
